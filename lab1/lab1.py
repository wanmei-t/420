# exercise 1
def exercise1():
    dividend = int (input('enter a number!!!'))
    divisor = int(input('enter a number!'))
    if dividend%2 != 0 :
        print( f' the number {dividend} is odd')
    else :
        if dividend%4 == 0 :
            print(f'the number {dividend} is a multiple of 4')
        else :
            print(f'the number {dividend} is an even number')
            
    if dividend%divisor == 0 :
        print(f'the dividend divides by the divisor evenly! good job!')
    else :
        print(f'the dividend DOES NOT divide by the divisor evenly')

def exercise2() :
    name = input('What\'s your name?')
    print(f'Hi {name}, how old are you?')
    age = int(input())
    gender = input('What is your gender?')
    year_100years = 100-age+2024
    print(f'So, mr/ms {name}, you will be 100 years old on {year_100years}')
    num = int (input('Enter a number between 3 and 13: '))
    print(f'So, mr/ms {name}, you will be 100 years old on {year_100years}\n' * num)

def exercise3() :
    name = input('What\'s your name?')
    score = int(input(f'Hi {name}, Enter your score [0, 100]'))
    grade = ''
    comment = ''
    if score>=95 :
        grade = 'A'
        comment = 'Brilliant!'
    if score<95 and score>=80 :
        grade = 'B'
        comment = 'Excellent!'
    if score<=70 and score>80 :
        grade = 'C'
        comment = 'Good job!'
    if score<70 and score>-60 :
        grade = 'D'
        comment = 'Medium...'
    if score<=59 :
        grade = 'F' 
        comment = 'Fail...'
    print(f'Yo {name}, you got {score} which is a {grade} grade. {comment}') 

def exercise4():
    endActivity = False
    while endActivity == False:
        num = int(input('Enter an integer: '))
        approx=0.5*num
        better=0.5*(approx+num/approx)
        while better != approx:
            approx=better
            better=0.5*(approx+num/approx)
        print(approx)
        answer = int(input('Do you want to try another number? 1 for yes, 0 for no: '))
        if answer == 0:
            endActivity = True

import random
def exercise5():
    num = int(input('Enter an integer between 1 and 100 inclusive: '))
    counter = 0
    randomNum = 0
    while randomNum != num:
        randomNum = random.randint(1, 100)
        counter += 1
    print(f'It took {counter} tries to randomize the same number as {num}.')

def exercise6():
    num = int(input('Enter a positive integer starting from 1: '))
    a = 0
    b = 1
    c = a
    while c < num:
        print(c)
        a = b
        b = c
        c = a+b

       
# exercise1()
# exercise2()
# exercise3()
# exercise4()
# exercise5()
# exercise6()