from flask import Flask

app = Flask(__name__)

#---------------------------------------------------
#Exercice 1
def exercise_1(list, num) :
    """ Returns the number of words in a list that has a length of 4 or more and them numth character and the last character of the word match """
    count = 0
    for word in list :
        if len(word) >= 4 and word[num - 1] == word[-1] :
            count += 1
    
    return count

# print(exercise_1(['abb', 'mnopp', 'helle', '1221', 'kariba', 'tiromani', 'me name', 'soo'], 2)) # returns 4

#---------------------------------------------------
#Exercice 2
def exercise_2(list1, list2) :
    """ Checks if two lists are mirroring each other """
    if len(list1) != len(list2) :
        return False
    
    for i in range(len(list1)) :
        if list1[i] != list2[len(list1) - 1 - i] :
            return False
        
    return True

# print(exercise_2([12,34,56,71,'a',0], [0,'a',71,56,34,12])) # returns True
# print(exercise_2(['a','b','d','c','e'], ['e','c','b','b','a'])) # returns False

#---------------------------------------------------
#Exercice 3
def exercise_3(setA, setB) :
    print("All elements of A and B:", setA.union(setB))
    print("\nAll common elements of A and B:", setA.intersection(setB))
    print("\nAll elements of A that is not in B:", setA.difference(setB))
    print("\nAll elements of B that is not in A:", setB.difference(setA))
    print("\nAll elements of A and B that are not in common:", setA.symmetric_difference(setB))
    print("\nIs A a superset of B:", setA.issuperset(setB))
    print("\nIs B a superset of A:", setB.issuperset(setA))
    print("\nIs A disjoint from B:", setA.isdisjoint(setB))
    print("\nIs B disjoint from A:", setB.isdisjoint(setA))
    print("\nIs A and B the same:", setA == setB )

# exercise_3({1,3,3,5,7,8,10,11}, {1, 2, 4, 6, 8,12})
# returns: 
# All elements of A and B: {1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12}
# All common elements of A and B: {8, 1}
# All elements of A that is not in B: {3, 5, 7, 10, 11}
# All elements of B that is not in A: {2, 4, 12, 6}
# All elements of A and B that are not in common: {2, 3, 4, 5, 6, 7, 10, 11, 12}
# Is A a superset of B: False
# Is B a superset of A: False
# Is A disjoint from B: False
# Is B disjoint from A: False
# Is A and B the same: False

#---------------------------------------------------
#Exercice 4 - dictionary
def exercise_4(sentence) :
    counts = {'a': 0, 'e': 0, 'i': 0, 'o': 0, 'u': 0, 'y': 0}
    for char in sentence:
        if char.lower() in counts:
            counts[char.lower()] += 1
    
    return counts

# print(exercise_4("hello world how are you")) # returns {'a': 1, 'e': 2, 'i': 0, 'o': 4, 'u': 1, 'y':1}

#---------------------------------------------------
#Exercice 5
def remove_duplicates(list) :
    """ Returns a list without duplicates """

    unique_items = []
    for item in list :
        if item not in unique_items :
            unique_items.append(item)
    return unique_items

def exercise_5(list) :
    groups = []
    group = []

    for i in range(len(list)) :
        if i == 0 or list[i] != list[i-1] :
            if group:
                groups.append(group)
                group = []
        group.append(list[i])
        if group :
            groups.append(group)

    return remove_duplicates(groups)

print(exercise_5([0, 0, 1, 1, 2, 3, 3, 4, 5, 5, 3, 3, 3, 7, 8, 9, 7, 7]))
# returns [[0, 0], [1, 1], [2], [3, 3], [4], [5, 5], [3, 3, 3], [7], [8], [9], [7, 7]]

#---------------------------------------------------
# #Exercice 6
def exercise_6(dict1, dict2) :
    dict3 = {}
    for key, value in dict1.items() :
        dict3[key] = value + dict3.get(key, 0)
    for key, value in dict2.items() :
        dict3[key] = value + dict3.get(key, 0)

    return dict3

# print(exercise_6({'a': 100, 'b': 200, 'c':300}, {'a': 300, 'b': 200, 'd':400})) # returns {'a': 400, 'b': 400, 'd': 400, 'c': 300}

#---------------------------------------------------
# #Exercice 7
def exercise_7(sentence) :
    counts = {}

    for char in sentence :
        counts[char] = counts.get(char, 0) + 1

    return counts

# print(exercise_7("lab2 exercises")) # returns {'l': 1, 'a': 1, 'b': 1, '2': 1, ' ': 1, 'e': 3, 'x': 1, 'r': 1, 'c': 1, 'i': 1, 's': 2}