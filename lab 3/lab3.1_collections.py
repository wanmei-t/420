# Lab 2.1 - Python Sequences
# NON GRADED LAB EXERCISE
# (prepared by Nasreddine Hallam)
# Due Date: At the end of the lab session
# No submission is required; however you should inform or show your results to your instructor

# Objectives:
#  Use the basic syntax of the Python language.
#  Use Python variables, constants, numeric, strings.
#  use Python flow constructs: operators, expressions.
#  work with statements and flow control: sequential, conditional, and repetitivestatement.

# -------------------
# Exercise 0
# -------------------
def extend(list1, list2) :
    """ Extends list1 with  list2 """

    return list1 + list2
# l1= [30,50,80,70]
# l2=[4,5,7,9]
# print(extend(l1, l2))  
# returns  [30, 50, 80, 70, 4, 5, 7, 9]
# -----------------------------------------------------

# -------------------
# Exercise 1
# -------------------
def count_occurences(list, target) :
    """ Returns the amount of occurences of a number in a list of numbers """
    count = 0
    for num in list :
        if num == target :
            count += 1
    
    return count
# print(count_occurences([1, 1, 2, 3, 1, 5, 1, 5, 6, 1], 1)) 
# returns 5
# -----------------------------------------------------

# -------------------
# Exercise 2
# -------------------
def remove_duplicates(list) :
    """ Returns a list without duplicates """

    unique_items = []
    for item in list :
        if item not in unique_items :
            unique_items = extend(unique_items, [item])
    return unique_items
# print(remove_duplicates([30, 30, 50, 80, 80, 70, 4, 5, 5, 7, 9])) 
# returns  [30, 50, 80, 70, 4, 5, 7, 9]
# -----------------------------------------------------

# -------------------
# Exercise 3
# -------------------
def find_longest_word(sentence) :
    """ Returns the first occurence of the longest word from a sentence """

    words = sentence.split(" ")
    longest_word = ""
    for word in words :
        if len(word) > len(longest_word) :
            longest_word = word
    
    return longest_word
# print(find_longest_word("Today is the most beautiful day ever with shining sunshine")) 
# returns beautiful
# -----------------------------------------------------

# -------------------
# Exercise 4
# -------------------
def extract_tld(list) :
    tlds = []
    for url in list :
        parts = url.split(".")
        tld = parts[-1].split("/")[0]
        tlds = extend(tlds, [tld])

    return tlds
# print(extract_tld(["www.google.com", "www.wikipedia.org", "https://dawsoncollege.omnivox.ca/intr/", "www.asp.net", "www.somesite.dz"])) 
# returns ['com', 'org', 'ca', 'net', 'dz']
# -----------------------------------------------------

# -------------------
# Exercise 5
# -------------------
def exercise_5(list1, list2) :
    """ Checks if two lists are mirroring each other """
    if len(list1) != len(list2) :
        return False
    
    for i in range(len(list1)) :
        if list1[i] != list2[len(list1) - 1 - i] :
            return False
        
    return True
# print(exercise_5([12,34,56,71,'a',0], [0,'a',71,56,34,12])) 
# returns True
# print(exercise_5(['a','b','d','c','e'], ['e','c','b','b','a'])) 
# returns False
# -----------------------------------------------------

# -------------------
# Exercise 6 - sets
# -------------------
def exercise_6(setA, setB) :
    print("All elements of A and B:", setA.union(setB))
    print("\nAll common elements of A and B:", setA.intersection(setB))
    print("\nAll elements of A that is not in B:", setA.difference(setB))
    print("\nAll elements of B that is not in A:", setB.difference(setA))
    print("\nAll elements of A and B that are not in common:", setA.symmetric_difference(setB))
    print("\nIs A a superset of B:", setA.issuperset(setB))
    print("\nIs B a superset of A:", setB.issuperset(setA))
    print("\nIs A disjoint from B:", setA.isdisjoint(setB))
    print("\nIs B disjoint from A:", setB.isdisjoint(setA))
    print("\nIs A and B the same:", setA == setB )
# exercise_6({1,3,3,5,7,8,10,11}, {1, 2, 4, 6, 8,12})
# returns: 
# All elements of A and B: {1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12}
# All common elements of A and B: {8, 1}
# All elements of A that is not in B: {3, 5, 7, 10, 11}
# All elements of B that is not in A: {2, 4, 12, 6}
# All elements of A and B that are not in common: {2, 3, 4, 5, 6, 7, 10, 11, 12}
# Is A a superset of B: False
# Is B a superset of A: False
# Is A disjoint from B: False
# Is B disjoint from A: False
# Is A and B the same: False
# -----------------------------------------------------

# -------------------
# Exercise 7
# -------------------
def exercise_7(list) :
    groups = []
    group = []

    for i in range(len(list)) :
        if i == 0 or list[i] != list[i-1] :
            if group:
                groups.append(group)
                group = []
        group.append(list[i])
        if group :
            groups.append(group)

    return remove_duplicates(groups)
# print(exercise_7([0, 0, 1, 1, 2, 3, 3, 4, 5, 5, 3, 3, 3, 7, 8, 9, 7, 7]))
# returns [[0, 0], [1, 1], [2], [3, 3], [4], [5, 5], [3, 3, 3], [7], [8], [9], [7, 7]]
# -----------------------------------------------------

# -------------------
# Exercise 8- dictionary
# -------------------
def exercise_8(sentence) :
    counts = {'a': 0, 'e': 0, 'i': 0, 'o': 0, 'u': 0, 'y': 0}
    for char in sentence:
        if char.lower() in counts:
            counts[char.lower()] += 1
    
    return counts
# print(exercise_8("hello world how are you")) 
# returns {'a': 1, 'e': 2, 'i': 0, 'o': 4, 'u': 1, 'y':1}
# -----------------------------------------------------

# -------------------
# Exercise 9
# -------------------
def exercise_9(dict1, dict2) :
    dict3 = {}
    for key, value in dict1.items() :
        dict3[key] = value + dict3.get(key, 0)
    for key, value in dict2.items() :
        dict3[key] = value + dict3.get(key, 0)

    return dict3
# print(exercise_9({'a': 100, 'b': 200, 'c':300}, {'a': 300, 'b': 200, 'd':400})) 
# returns {'a': 400, 'b': 400, 'd': 400, 'c': 300}
# -----------------------------------------------------