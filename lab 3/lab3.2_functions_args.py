# Lab 2.1 - # function Parameters
# NON GRADED LAB EXERCISE
# (prepared by Nasreddine Hallam)
# Due Date: At the end of the lab session
# No submission is required; however you should inform or show your results to your instructor


#Exercise 0 (basic parameters)
def test_normal_args(object) :
    print("Argument: ", object, " --> ", type(object))
# test_normal_args([1,2,3])
# returns Argument:  [1, 2, 3]  -->  <class 'list'>
# test_normal_args((1,2,3))
# returns Argument:  (1, 2, 3)  -->  <class 'tuple'>
# test_normal_args('test')
# returns Argument:  test  -->  <class 'str'>
# test_normal_args(123)     
# returns Argument:  123  -->  <class 'int'>
# test_normal_args(123, 123) 
# returns TypeError: test_normal_args() takes 1 positional argument but 2 were given
# test_normal_args() 
# returns TypeError: test_normal_args() missing 1 required positional argument: 'object'
    
#----------------------------------------------------------------------------------------

#Exercise 1 (basic parameters)
def add(item1, item2) :
    print(item1 + item2)
# add("hi", "lls")
# returns hills
# add(3, 2)
# returns 5
# add(3.14, 9.81)
# returns 12.950000000000001
# add([1,2,3,2], [7,8,7])
# returns [1, 2, 3, 2, 7, 8, 7]
# add(('spring','winter'), ('spring', 'summer', 'fall'))
# returns ('spring', 'winter', 'spring', 'summer', 'fall')
# add(range(5), range(6))
# returns TypeError: unsupported operand type(s) for +: 'range' and 'range'
# add({1, 2, 3, 4, 5}, {'a', 'b', 'c'})
# returns TypeError: unsupported operand type(s) for +: 'set' and 'set'
# add({'apple': 2, 'banana': 3, 'orange': 5}, {'name': 'John', 'age': 25, 'city': 'New York'})
# returns TypeError: unsupported operand type(s) for +: 'dict' and 'dict'

#----------------------------------------------------------------------------------------

#Exercise 2 (default parameters: arg=value)
def add_def(a = 5, b = 10) :
    print(a + b)
# add_def()  
# returns 15
# add_def(7, 11, 5) 
# returns TypeError: add_def() takes from 0 to 2 positional arguments but 3 were given
# add_def(7, 11)
# returns 18
# add_def(7)
# returns 17    
# add_def(, 11)
# returns  SyntaxError: invalid syntax
# add_def(b = 11)
# returns 16
    
def add_def2(a = 5, b = 10, c = 20) :
    print(a + b + c)
# add_def2()
# returns 35
# add_def2(20)
# returns 50
# add_def2(20, 30)
# returns 70
# add_def2(20, 30, 40)
# returns 90
# add_def2(20, 30, 40, 50)
# returns TypeError: add_def2() takes from 0 to 3 positional arguments but 4 were given

#----------------------------------------------------------------------------------------

#Exercise 3 (variable parameters: *)
def adding(*args) :
    builder = ""
    for arg in args :
        builder = builder + f"{arg}"
    print(args)
# adding(1, 2, 3)
# adding('hi', 'lls')
# adding('hi', 'lls', ' has', ' eyes')
# adding(['q',1], [(2,3), 'hello'])
# adding()
# adding('hi', ['q',1], 1)
# adding({'apple': 2, 'banana': 3, 'orange': 5}, {'name': 'John', 'age': 25, 'city': 'New York'})
    
#----------------------------------------------------------------------------------------

#Exercise 4 (keyworded args: **)
def form_url(**kwargs) :
    scheme = kwargs.get('scheme', 'http')
    host = kwargs.get('host', 'www')
    domain_name = kwargs.get('domain_name', '') 

    print(f"{scheme}://{host}.{domain_name}")
# form_url(scheme='https', host='www', domain_name='cbc.ca')
# returns https://www.cbc.ca
# form_url(domain_name='dawsoncollege.qc.ca', host='sonic' )
# returns http://sonic.dawsoncollege.qc.ca