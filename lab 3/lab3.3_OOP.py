# Lab 3.3 - OOP
# By Nasr

# a deck consists of  52 cards, each card has a value (1 to 13)
# and one of the 4 suits: 
# spade (♠),heart (♥), diamond (♦),  and club (♣) with Unicodes 
# U+00002660  U+00002665  U+00002666  U+00002663 respectively.

#--------------------------part 1---------------------------------------
# create a dictionary called suit that stores all the 4 suits
suits = {"spade": "♠", "heart": "♥", "diamond": "♦", "club": "♣"}

class Card :
    def __init__(self, suit, value) :
        if suit not in suits :
            raise ValueError("Invalid suit")
        if value < 1 or value > 13 :
            raise ValueError("Invalid value")
        self.suit = suit
        self.value = value

    def show(self) :
        print(self.__str__())
    
    def __str__(self) :
        return f"{suits[self.suit]} | {self.value}"

if __name__ == "__main__" :
    card = Card("spade", 3)
    card.show()

#--------------------------part 2---------------------------------------
import random
class Deck :
    def __init__(self) :
       self.cards = []
       self.setup_fields()
       self._initial_build()

    def setup_fields(self) :
        self._values = [i for i in range(1, 14)]
        self._suits = ["spade", "heart", "diamond", "club"]

    def _initial_build(self) :
        for value in self._values :
            for suit in self._suits :
                self.cards.append(Card(suit, value))

    def shuffle(self) :
        random.shuffle(self.cards)
    
    def show(self) :
        for card in self.cards :
            card.show()

if __name__ == "__main__" :
    deck = Deck()
    deck.show()
    deck.shuffle()
    deck.show()