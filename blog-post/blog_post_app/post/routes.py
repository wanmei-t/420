from flask import abort, Blueprint, render_template
from markupsafe import escape
from blog_post_app.post.posts import posts_list

post_bp = Blueprint(
    "post_bp",
    __name__,
    template_folder = "templates",
    static_folder = "static"
)

@post_bp.route("/post/<int:post_id>")
def post(post_id) :
    """ Post """
    post_id = int(escape(post_id))
    post_item = None
    for post in posts_list :
        if post["id"] == post_id :
            post_item = post
            break
    
    if post_item is None :
        return abort(404)

    context = {
        "page_title": f"Post {post_id}",
        "main_heading": f"Post {post_id} info",
        "post": post_item
    }
    return render_template("post.html", context=context)

@post_bp.route("/list-all-posts")
def list_all_posts() :
    """ List all posts """
    context = {
        "page_title": "Posts",
        "main_heading": "List of All Posts",
        "posts": posts_list
    }
    return render_template("list-all-posts.html", context=context)