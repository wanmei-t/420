from flask import Blueprint, render_template, redirect, url_for, request, flash
from blog_post_app.author.forms import RegistrationForm, LoginForm

author_bp = Blueprint(
    "author_bp",
    __name__,
    template_folder = "templates",
    static_folder = "static"
)

@author_bp.route("/register", methods = ["GET", "POST"])
def registration() :
    """ Register """
    register_form = RegistrationForm()
    if request.method == "POST":
        if register_form.validate_on_submit() :
            flash("Successfully registered!", "success-bg")
            return redirect(url_for("main_bp.home"))
    context = {
        "page_title": "Registration",
        "form": register_form
    }
    return render_template("registration.html", context=context)
    

@author_bp.route("/login", methods = ["GET", "POST"])
def login() :
    """ Login """
    login_form = LoginForm()
    if request.method == "POST":
        if login_form.validate_on_submit() :
            flash("Successfully logged in!", "success-bg")
            return redirect(url_for("main_bp.home"))
    context = {
        "page_title": "Login",
        "form": login_form
    }
    return render_template("login.html", context=context)