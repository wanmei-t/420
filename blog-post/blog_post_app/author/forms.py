""" Flask Form """
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email

class RegistrationForm(FlaskForm) :
    """ Registration Form """
    username = StringField("Username: ", validators=[DataRequired(), Length(min=2, max=25)])
    email = StringField("Email: ", validators=[DataRequired(), Email()])
    password = PasswordField("Password: ", validators=[DataRequired()])
    confirm_password = PasswordField("Confirm password: ", validators=[DataRequired()])
    submit = SubmitField("Register")

class LoginForm(FlaskForm) :
    """ Login Form """
    username = StringField("Username: ", validators=[DataRequired(), Length(min=2, max=25)])
    password = PasswordField("Password: ", validators=[DataRequired()])
    remember_me = BooleanField("Remember me")
    submit = SubmitField("Login")