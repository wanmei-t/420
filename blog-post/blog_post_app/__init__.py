from flask import Flask, render_template
from config import ConfigDev

def create_app(class_config=ConfigDev) :
    """ Create an instance of the blog post app """
    app = Flask(__name__)
    app.config.from_object(class_config)
    
    from blog_post_app.author.routes import author_bp
    from blog_post_app.main.routes import main_bp
    from blog_post_app.post.routes import post_bp
	
    app.register_blueprint(author_bp)
    app.register_blueprint(main_bp)
    app.register_blueprint(post_bp)
    
    app.register_error_handler(404, page_not_found)
	
    return app

def page_not_found(e) :
    """ Page Not Found """
    context = {
        "page_title": "404",
        "main_heading": "Page Not Found - Error 404"
    }
    return render_template("404.html", context=context), 404