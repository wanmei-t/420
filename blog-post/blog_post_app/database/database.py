# import oracledb
# import config_db
# import os

# class Database:
#     def __init__(self, auto_commit = True):
#         self.open_connection(auto_commit)

# # --------------------------------------------------------------------
# # ------- Operations that handles the connection with the database
#     def open_connection(self, auto_commit) :
#         try :
#             self.close_connection()
#         except Exception as e :
#             pass
#         self.__connection = self.__connect()
#         self.__connection.autocommit = auto_commit

#     def __connect(self):
#         return oracledb.connect(
#             host = config_db.host, 
#             user = config_db.user, 
#             password = config_db.password, 
#             service_name = config_db.server_name, 
#             port = config_db.port
#         )
    
#     def close_connection(self):
#         if self.__connection is not None :
#             self.__connection.close()
#             self.__connection = None

#     def db_conn(self):
#         return self.__connection

#     def run_sql_script(self, script_path):
#         if os.path.exists(script_path):
#             self.__run_file(script_path)
#         else:
#             print('Invalid Path')    

#     def __run_file(self, file_path):
#         try:
#             self.open_connection(True)
#             with self.db_conn().cursor() as cursor:
#                 with open(file_path, "r") as f:
#                     script = f.read()

#                 statements = script.split(';')

#                 for statement in statements:
#                     print(statement)
#                     try:
#                         cursor.execute(statement)
#                     except Exception as e:
#                         print(f"Error executing SQL statement: {e}")
#         except Exception as e:
#             print(f"Error reading SQL script file: {e}")
#         finally :
#             self.close_connection()

# # --------------------------------------------------------------------
# # ------- Operations that handles the data of the database
#     def get_addresses(self, cond):
#         ''' method to list all (if cond is True) or some address books based on cond'''
#         try :
#             self.open_connection(True)
#             with self.db_conn().cursor() as cursor :
#                 if (cond == True) :
#                     query = "SELECT * FROM ADDRESS_BOOK"
#                 elif (cond != None) :
#                     query = f"SELECT * FROM ADDRESS_BOOK WHERE {cond}"
#                 else :
#                     raise Exception("Invalid condition!")
                
#                 cursor.execute(query)
#                 results = cursor.fetchall()
#                 addresses = []
#                 for row in results :
#                     addresses.append(Address(row[1], row[2], row[3], row[4]))

#                 return addresses
#         except Exception as e :
#             print(f"An error occured: {e}")
#         finally :
#             self.close_connection()

#     def get_notes(self):
#         ''' method to list all notes '''
#         try :
#             self.open_connection(True)
#             with self.db_conn().cursor() as cursor :
#                 query = "SELECT * FROM NOTE"
#                 cursor.execute(query)
#                 results = cursor.fetchall()
#                 notes = []
#                 for row in results :
#                     notes.append(Note(row[0], row[2]))

#                 return notes
#         except Exception as e :
#             print(f"An error occured: {e}")
#         finally :
#             self.close_connection()

#     def get_notes_user(self, user_name):
#         ''' method to list all notes of a given user '''
#         try :
#             self.open_connection(True)
#             with self.db_conn().cursor() as cursor :
#                 query = "SELECT Note_Id, Content FROM NOTE INNER JOIN ADDRESS_BOOK USING (Author_Id) WHERE User_Name = :user_name"
#                 cursor.execute(query, (user_name,))
#                 results = cursor.fetchall()
#                 notes = []
#                 for row in results :
#                     notes.append(Note(row[0], row[1]))

#                 return notes
#         except Exception as e :
#             print(f"An error occured: {e}")
#         finally :
#             self.close_connection()

#     def get_note_with_note_id(self, note_id):
#         ''' method to list all notes of a given user '''
#         try :
#             self.open_connection(True)
#             with self.db_conn().cursor() as cursor :
#                 query = "SELECT * FROM NOTE WHERE Note_Id = :note_id"
#                 cursor.execute(query, (note_id,))
#                 results = cursor.fetchall()
#                 notes = []
#                 for row in results :
#                     notes.append(Note(row[0], row[2]))

#                 return notes[0]
#         except Exception as e :
#             print(f"An error occured: {e}")
#         finally :
#             self.close_connection()

#     def add_new_address(self, address):
#         '''  method to add a new address from an Address object'''
#         self.add_new_address_from_form(address.name, address.street, address.city, address.province)

#     def add_new_address_from_form(self, user_name, street, city, province):
#         '''  method to add a new address, data coming fro a user input form'''
#         try :
#             self.open_connection(True)
#             with self.db_conn().cursor() as cursor :
#                 query = "INSERT INTO ADDRESS_BOOK (User_Name, Street, City, Province) VALUES (:User_Name, :Street, :City, :Province)"
#                 cursor.execute(query, (user_name, street, city, province))
#         except Exception as e :
#             print(f"An error occured: ", e)
#         finally :
#             self.close_connection()


# # --------------------------------------------------------------------
# # ------- Global variables and running in isolation
# db = Database()

# if __name__ == "__main__":
#     db.run_sql_script("schema.sql")