from flask import abort, Blueprint, redirect, render_template, url_for
from markupsafe import escape

main_bp = Blueprint(
    "main_bp",
    __name__,
    template_folder = "templates",
    static_folder = "static"
)

@main_bp.route("/")
@main_bp.route("/home")
def home() :
    """ Home """
    context = {
        "page_title": "Home",
        "main_heading": "Welcome to my home page!"
    }
    return render_template("home.html", context=context)

@main_bp.route("/about")
def about() :
    """ About """
    context = {
        "page_title": "About",
        "main_heading": "Welcome to my about page!"
    }
    return render_template("about.html", context=context)

@main_bp.route("/page-x/<string:choice>")
def page_x(choice) :
    """ Page x """    
    choice = escape(choice)

    if choice == "h" :
        return redirect(url_for("main_bp.home"))
    elif choice == "a" :
        return redirect(url_for("main_bp.about"))
    else :
        return abort(404)