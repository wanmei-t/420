from flask import Blueprint

blog_post_bp = Blueprint(
    "blog_post_bp", 
    __name__,
    template_folder = "templates",
    static_folder = "static"
)