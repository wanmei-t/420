from blog_post_app import create_app
from config import ConfigDev, ConfigProd

app = create_app(ConfigDev)

if __name__ == "__main__" :
    app.run(port=5000, debug=True)