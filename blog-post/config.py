import os

class Config :
    """ Basic configuration which contains all the common configurations """
    SECRET_KEY = os.environ.get("SECRET_KEY", "skgneiprhgpf")
    DEBUG = True
    TESTING = True
    FLASK_ENV = "Development"
	
class ConfigDev(Config) :
    """ Configuration for development """
    TESTING = False
	
class ConfigProd(Config) :
    """ Configuration for production """
    DEBUG = False
    TESTING = False
    FLASK_ENV = "Production"