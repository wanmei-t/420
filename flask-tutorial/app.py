from flask import Flask

app = Flask(__name__)

@app.route("/")
@app.route("/home")
def home_page():
    return "<h1>Flask Tutorial</h1>"

@app.route("/about/<name>/<course>/<year>")
@app.route("/a-propos")
def about_page(name, course, year):
    return f''' 
        <li>Name = {name} </li> 
        <li>Course = {course} </li> 
        <li>Year = {year} </li> 
    '''